package com.example.sandeepsinha.appdummy;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public Button disableCamera;
    public Button enableCamera;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        disableCamera = findViewById(R.id.disable_camera);
        enableCamera= findViewById(R.id.enable_camera);
        Context context = getApplicationContext();
        CharSequence enableText = "camera enabled";
        CharSequence disableText = "camera disable";
        int duration = Toast.LENGTH_SHORT;

        final Toast toastEnable = Toast.makeText(context, enableText, duration);
        final Toast toastDisable = Toast.makeText(context, disableText, duration);


       final DevicePolicyManager  mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

       disableCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDevicePolicyManager.setCameraDisabled(getActiveComponentName(), true);
                toastDisable.show();
            }
        });

       enableCamera.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mDevicePolicyManager.setCameraDisabled(getActiveComponentName(),false);
               toastEnable.show();
           }
       });

    }


    public class DemoAdminReceiver extends DeviceAdminReceiver {
        @Override
        public void onEnabled(Context context, Intent intent) {
            super.onEnabled(context, intent);

        }
    }



    private ComponentName getActiveComponentName() {

        final DevicePolicyManager  mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName componentName = null;

        List<ComponentName> activeComponentList = mDevicePolicyManager.getActiveAdmins();

        Iterator<ComponentName> iterator = activeComponentList.iterator();

        while (iterator.hasNext()) {

            componentName = (ComponentName) iterator.next();
        }
        return componentName;
    }




}

